function MyQueue() {
  let collections = [];

  this.print = () => collections;

  this.enqueue = element => collections.push(element);

  this.dequeue = () => collections.shift();

  this.front = () => collections[0];

  this.isEmpty = () => this.size === 0;

  this.size = () => collections.length;
}

let customQueue = new MyQueue();

customQueue.enqueue(1);
customQueue.enqueue(2);
customQueue.enqueue(3);
customQueue.enqueue(4);
console.log(customQueue.print());
customQueue.dequeue();
console.log(customQueue.print());
console.log(customQueue.front());
console.log(customQueue.size());
console.log(customQueue.isEmpty());
