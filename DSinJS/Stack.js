let Stack = function () {
  this.count = 0;
  this.storage = {};

  this.push = function (value) {
    this.storage[this.count] = value;
    this.count++;
  };

  this.pop = function () {
    if (this.count === 0) {
      return "stack under flow";
    }

    this.count--;
    let remove = this.storage[this.count];
    delete this.storage[this.count];

    return remove;
  };

  this.size = function () {
    return this.count;
  };

  this.peek = function () {
    return this.storage[this.count - 1];
  };

  this.log = function () {
    return this.storage;
  };
};

let stack = new Stack();

stack.push(10);
stack.push(20);
stack.push(30);
console.log(stack.log());
console.log(stack.size());
console.log(stack.pop());
console.log(stack.peek());
console.log(stack.size());
