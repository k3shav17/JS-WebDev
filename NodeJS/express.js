const express = require("express");
const app = express();

app.get("/", function (req, res) {
  res.send("Hello Node");
});

app.get("/user", function (req, res) {
  res.send("Hello user");
});
app.listen(8080, function (req, res) {
  console.log("Server is Running on 8080");
});

app.get("/user/:id", function (req, res) {
  const id = req.params.id;
  res.send("hello " + id);
});
