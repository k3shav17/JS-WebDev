const express = require("express");
const mongoose = require("mongoose");

const url =
  "mongodb+srv://bookman:dbpass0489@realmcluster.6jpgl.mongodb.net/node-crud?retryWrites=true&w=majority";

const app = express();

mongoose.connect(url);
const connection = mongoose.connection;

connection.on("open", () => {
  console.log("connected to db");
});

app.use(express.json());

const alienRouter = require("./routers/alien.js");
app.use("/aliens", alienRouter);

app.listen(9000, () => {
  console.log("server has started");
});
