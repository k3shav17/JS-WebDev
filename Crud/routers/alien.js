const express = require("express");
const { MongoClient } = require("mongodb");

const router = express.Router();
const Model = require("../models/model.js");

router.get("/", async (req, res) => {
  try {
    const aliens = await Model.find();
    res.json(aliens);
  } catch (err) {
    res.send("error " + err);
  }
});

router.get("/name/:name", async (req, res) => {
  try {
    var alien = await Model.findOne({ name: req.params.name });
    res.json(alien);
  } catch (err) {
    res.send("error " + err);
  }
});

router.get("/:id", async (req, res) => {
  try {
    const alienId = await Model.findById(req.params.id);
    res.json(alienId);
  } catch (err) {
    res.send("error " + err);
  }
});
router.post("/addAlien", async (req, res) => {
  const models = new Model({
    name: req.body.name,
    tech: req.body.tech,
    sub: req.body.sub,
  });
  try {
    const add = await models.save();
    res.json(add);
  } catch (err) {
    res.send("error " + err);
  }
});

module.exports = router;
