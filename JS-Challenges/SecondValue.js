function secondValue(arr) {
  if (arr.length === 1) return arr[0];

  let temp = [];
  arr.sort((a, b) => {
    return a - b;
  });

  if (arr.length === 2) return arr;

  if (arr.length === 3) return arr[1];

  temp.push(arr[1]);
  temp.push(arr[arr.length - 2]);

  return temp;
}

console.log(secondValue([1]));
console.log(secondValue([4, 2]));
console.log(secondValue([11, 44, 22]));
console.log(secondValue([3, 2, 88, 3, -11, 67, 7]));
