const date1 = new Date('May 27, 2021');
const date2 = new Date('June 5, 2023');

const unixDate1 = date1.getTime();
const unixDate2 = date2.getTime();

const oneDay = 1000 * 60 * 60 * 24;
let time = Math.abs(unixDate2 - unixDate1);
time /= oneDay;
time = Math.round(time);
console.log(time);

const getDays = (date1, date2) =>
  Math.round(Math.abs((date2 - date1) / (1000 * 3600 * 24)));

console.log(getDays(date1, date2));
