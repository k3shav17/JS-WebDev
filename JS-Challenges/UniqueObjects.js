let products = [
  {
    title: "Iphone 8",
    company: "apple",
  },
  {
    title: "Iphone 7",
    company: "apple",
  },
  {
    title: "Galaxy",
    company: "samsung",
  },
  {
    title: "HTC Phone",
    company: "htc",
  },
  {
    title: "Iphone Xs",
    company: "apple",
  },
];

function getUnique(arr) {
  let tempArr = arr.map((name) => name.company);
  return [...new Set(tempArr)];
}

console.log(getUnique(products));
