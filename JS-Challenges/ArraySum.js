function arraySum(arr) {
  let max = -1;
  let sum = 0;

  for (let val of arr) {
    if (val > max) {
      max = val;
    }
  }

  for (let i = 0; i < arr.length; i++) {
    if (arr[i] !== max) {
      sum += arr[i];
    } 
  }
  return sum === max;
}

console.log(arraySum([1, 2, 4, 6, 13]));
console.log(arraySum([1, 2, 4, 34, 22]));
