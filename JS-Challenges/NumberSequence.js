// count sequences, three numbers, first equals to third
// and middle one different, all three numbers equal does not count [4, 4, 4]

const first = [8, 6, 8, 6, 7, 4, 7];
const second = [4, 5, 5, 7, 5, 5, 9];
const third = [3, 3, 2, 8, 7, 6, 5];

// normal approach
const sequence = arr => {
  let triplets = [];
  for (let i = 0; i < arr.length - 2; i++) {
    if (arr[i] === arr[i + 2]) {
      let temp = [];
      for (let j = i; j <= i + 2; j++) {
        temp.push(arr[j]);
      }
      triplets.push(temp);
    }
  }
  return triplets.length;
};

// with reduce method
const sequencReduce = arr => {
  return arr.reduce((total, item, index) => {
    const match = item === arr[index + 2] && item !== arr[index + 1];

    if (match) return total + 1;
    return total;
  }, 0);
};

// one liner
const oneLine = arr =>
  arr.reduce(
    (total, item, index) =>
      total + (item === arr[index + 2] && item !== arr[index + 1]),
    0
  );

// total + (item === arr[index + 2] && item !== arr[index + 1]),
// this will be evaluated to be either 1 or 0 based on the condition in line 37

console.log(sequencReduce(third));
console.log(oneLine(first));
