function mathSequence(nums) {
  let arith = new Set();
  let geo = new Set();

  for (let i = 1; i < nums.length; i++) {
    arith.add(nums[i] - nums[i - 1]);
    geo.add(nums[i] / nums[i - 1]);
  }

  if (arith.size === 1) return "arithmetic";
  if (geo.size === 1) return "geometric";
  return -1;
}

console.log(mathSequence([2, 4, 6, 8]));
console.log(mathSequence([3, 9, 27]));
console.log(mathSequence([1, 4, 5, 9]));
