// function example(a, b) {
//   return function (c, d) {
//     return function (e, f) {
//       return a * c * e + b * d * f;
//     };
//   };
// }

const example = (a, b) => (c, d) => (e, f) => a * c * e + b * d * f;
example(1, 2)(3, 4)(5, 6);
example(1, 2)(1, 2)(3, 4);
example(1, 2)(0, 1)(0, 5);
