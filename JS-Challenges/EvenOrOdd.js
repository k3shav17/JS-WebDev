// odd or even
//
//
// sum(56) - odd
// 5 + 6 = 11
// 11 % 2 = 1
//

const sum = number => {
  const items = [...number.toString()];

  let total = items.reduce((total, item) => {
    total += Number(item);
    return total;
  }, 0);

  return total % 2 === 0 ? 'Even' : 'Odd';
};

console.log(sum(55));

// one liner
const oneline = number =>
  [...number.toString()].reduce((total, item) => total + Number(item), 0) %
    2 ===
  0
    ? 'Even'
    : 'Odd';
console.log(oneline(55));
