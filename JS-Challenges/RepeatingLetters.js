function countLetters(str) {
  let temp = str.split("");
  let letters = [];
  let count = 1;

  for (let i = 0; i < temp.length; i++) {
    if (temp[i] === temp[i + 1]) {
      count++;
    } else {
      let value = `${count}${temp[i]}`;
      letters = [...letters, value];
      count = 1;
    }
  }
  return letters.join("");
}

console.log(countLetters("ffffeertttooo"));
