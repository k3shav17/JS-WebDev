function checkPalindrome(str) {
  str = str.trim().toLowerCase();

  for (let i = 0; i < str.length; i++) {
    if (str.charAt(i) !== str.charAt(str.length - 1 - i)) return false;
    return true;
  }
}

console.log(checkPalindrome("Was it a car or a cat I saw"));
console.log(checkPalindrome("Red -rum-, sir,-is-murder"));
console.log(checkPalindrome("I got up early this morning"));
