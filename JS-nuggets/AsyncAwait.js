// Async / Await
// Async must be present, always returns a promise
// Await waits till promise is settled
// error handling - try/catch block
//

const users = [
  { id: 1, name: 'john' },
  { id: 2, name: 'susan' },
  { id: 3, name: 'anna' },
];
const articles = [
  { userId: 1, articles: ['one', 'two', 'three'] },
  { userId: 2, articles: ['four', 'five'] },
  { userId: 3, articles: ['six', 'seven', 'eight', 'nine'] },
];

function getUser(name) {
  return new Promise((resolve, reject) => {
    const user = users.find(user => user.name === name);

    if (user) {
      resolve(user);
    } else reject(`No such user with name: ${name}`);
  });
}

function getArticles(userId) {
  return new Promise((resolve, reject) => {
    const userArticles = articles.find(user => user.userId === userId);

    if (userArticles) resolve(userArticles);
    else reject(`Wrong user ${userId}`);
  });
}

const getData = async () => {
  try {
    const user = await getUser('john');
    const result = await getArticles(user.id);
    console.log(result);
  } catch (error) {
    console.log(error);
  }
};

getData();

getUser('anna')
  .then(user => getArticles(user.id))
  .then(articles => console.log(articles))
  .catch(err => console.log(err));
