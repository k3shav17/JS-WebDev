// Fetch API - browser API for HTTP (ajax) requests
// Default - GET requests, Supports other methods as well
// returns a Promise

import fetch from 'node-fetch';

const url = 'https://www.course-api.com/react-tours-project';

console.log(fetch(url));

// fetch(url)
//   .then(res => res.json().then(data => console.log(data.length)))
//   .catch(err => console.log(err));

const info = async () => {
  try {
    const response = await fetch(url);
    const data = await response.json();
    // console.log(data);
    return data;
  } catch (error) {
    console.log(error);
  }
};

console.log(info().then());
