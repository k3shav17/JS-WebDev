// Faster/easier way to access/unpack variables from an array, objects

const fruits = ["orange", "banana", "lemon"];
const friends = ["john", "peter", "bob", "anna", "kelly"];

const fruit0 = fruits[0];
const fruit1 = fruits[1];
const fruit2 = fruits[2];
// this process is redundant
console.log(fruit0, fruit1, fruit2);

const [b, , , a] = friends;

console.log(b, a);

let first = "bob";
let second = "john";

[s, f] = [first, second];
console.log(f, s);
