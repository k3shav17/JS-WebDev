// Timestamps

console.log(new Date());

console.log(Date.now());

setTimeout(() => {
  console.log(Date.now());
}, 1000);

let people = [];
people = [...people, { id: Date.now(), name: 'peter' }];

setTimeout(() => {
  console.log(people);
}, 1000);

console.log(new Date(1660153445056));

const now = Date.now();
const futureDate = new Date(now + 1000 * 60);
console.log(futureDate);
console.log(new Date());
