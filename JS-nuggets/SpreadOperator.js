// Spread Operator
// Allows and iterable to spread/expand individually inside reciever
// Split into single items and copy them

const udemy = 'udemy';
const letters = [...udemy];
console.log(letters);

const boys = ['john', 'peter', 'bob'];
const girls = ['susan', 'anna'];

const bestFriend = 'arnold';

const friend = [...boys, bestFriend, ...girls];
console.log(friend);

// reference

// copy
const newFriends = [...friend];
newFriends[0] = 'nancy';
console.log(friend);
console.log(newFriends);

// ES2018 - ES8 objects

const person = { name: 'john', job: 'developer' };

const newPerson = { ...person, city: 'chicago', name: 'peter' };
console.log(person);
console.log(newPerson);
