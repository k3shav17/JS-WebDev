// Rest operators
// gathers/collects items
// destructuring, functions
// placement important, careful with same name
// rest when declare function, spread when invoke the function

// arrays
const fruits = ['orange', 'banana', 'lemon'];
const [first, ...rest] = fruits;
console.log(first, rest);
const specificFruit = rest.find(fruit => fruit === 'banana');
console.log(specificFruit);

// objects
const person = { name: 'john', lastName: 'smith', job: 'developer' };
const { name, ...restPerson } = person;
console.log(name, restPerson);

// functions

const getAverage = (name, ...scores) => {

  console.log(scores);
  const avg = scores.reduce((acc, curr) => {
    return (acc += curr);
  }, 0) / scores.length;
  console.log(avg);
};

const testScores = [23, 45, 67, 89];

getAverage(person.name, ...testScores);
// getAverage(person.name, 89, 83, 70);
