// Array.from - NOT ON PROTOTYPE
//
// from - return Array object from an object
// with a length property or an iterable object
// from - turns array-like/ish into array - string, nodeList, Set

const udemy = 'udemy';

const temp = Array.from(udemy);
console.log(temp);
