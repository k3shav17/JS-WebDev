const person = {
  name: "John",
};

// dot notation
console.log(person.name);

// square bracket notation

const items = {
  "featured-items": ["item1", "item2"],
};

console.log(items["featured-items"]);
console.log(person["name"]);
