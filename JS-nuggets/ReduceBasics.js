// Reduce
// iterates, callback function
// reduces to a single value - number, array, object
// 1st parameter ('acc') - total of all calculations
// 2nd parameter ('curr') = current iteration/value

const people = [
  {
    name: "bob",
    age: 20,
    position: "developer",
    salary: 100,
  },
  {
    name: "anna",
    age: 25,
    position: "designer",
    salary: 300,
  },
  {
    name: "susy",
    age: 30,
    position: "the boss",
    salary: 400,
  },
  {
    name: "John",
    age: 23,
    position: "intern",
    salary: 10,
  },
];

const totalSalary = people.reduce((acc, curr) => {
  acc += curr.salary;
  return acc;
}, 0);

console.log(totalSalary);
