// Filter returns new array, can manipulate  the size of the array (unlike map), return based on condition

// Find - returns single instance (object), returns first match, if no match - undefined

const people = [
  {
    name: "bob",
    age: 20,
    position: "developer",
  },
  {
    name: "anna",
    age: 25,
    position: "designer",
  },
  {
    name: "susy",
    age: 30,
    position: "the boss",
  },
  {
    name: "John",
    age: 23,
    position: "intern",
  },
];

// filter

const young = people.filter((person) => person.age < 25);
console.log(young);

const developers = people.filter((person) => person.position === "developer");
console.log(developers);

// no match

const seniorDevs = people.filter((item) => item.position === "senior Dev");
console.log(seniorDevs);

// find

const bob = people.find((person) => person.name === "bob");
console.log(bob.position);
console.log(bob);
// no match

const peter = people.find((person) => person.name === "peter");
console.log(peter);

// multiple matches - always returns first match
const randomPerson = people.find((person) => person.age < 35);
console.log(randomPerson);

const anna = people.filter((person) => person.name === "anna");
console.log(anna.position);
