// Promises
//
// async await
// consume/use promises
//
// Pending, Rejected, Fulfilled

const promise = new Promise((resolve, reject) => {
  resolve('hello world');
  reject('There was an error');
});

promise.then(data => console.log(data)).catch(err => console.log(err));

const value = 2;

const randomPromise = new Promise((resolve, reject) => {
  const random = Math.floor(Math.random() * 3);

  if (random === value) resolve('Hurray!');
  else reject('Nay!');
});

randomPromise.then(data => console.log(data)).catch(err => console.log(err));
