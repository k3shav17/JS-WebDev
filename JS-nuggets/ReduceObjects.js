const cart = [
  {
    title: "Samsung Galaxy S7",
    price: 599.99,
    amount: 1,
  },
  {
    title: "Google Pixel",
    price: 499.99,
    amount: 2,
  },
  {
    title: "Xiaomi Redmi Note 2",
    price: 199.99,
    amount: 4,
  },
  {
    title: "Xiamoi Redmi Note 5",
    price: 299.99,
    amount: 3,
  },
];

let { totalItems, cartTotal } = cart.reduce(
  (total, cartItem) => {
    const { amount, price } = cartItem;

    // count items
    total.totalItems += amount;
    // count sum
    total.cartTotal += amount * price;

    return total;
  },
  { totalItems: 0, cartTotal: 0 }
);

cartTotal = parseFloat(cartTotal.toFixed(2));

console.log(totalItems, cartTotal);
