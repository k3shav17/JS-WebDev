function makeUppercase(value) {
  console.log(value.toUpperCase());
}
makeUppercase('peter');

function reverseString(value) {
  console.log(value.split('').reverse().join(''));
}

function handleName(name, cb) {
  const fullName = `${name} parker`;
  cb(fullName);
}

handleName('peter', makeUppercase);
handleName('peter', reverseString);

handleName('bob', value => console.log(value + ' the builder'));


// array methods, setTimeout, event listeners etc
