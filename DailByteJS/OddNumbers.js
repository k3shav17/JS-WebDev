/*Given two non-negative integers low and high, return the total count of odd numbers between them (inclusive).

Ex: Given the following low and high…

low = 1, high = 3, return 2 (1 and 3 are both odd).

Ex: Given the following low and high…

low = 1, high = 10, return 5.*/

const readline = require("readline-sync");

console.log(
  "enter the low and high values for which you want find the no of odd numbers"
);

let low = Number(readline.question());
let high = Number(readline.question());

let count = 0;
for (let i = low; i <= high; i++) {
  if (i % 2 != 0) {
    count++;
  }
}

console.log(count);
