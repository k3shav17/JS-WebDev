/* Binary search is searching algorith for finding an element's position in a sorted array.
 * In this approach, the element is always searched in the middle of a portion of an array.
 *
 * Binary search can be implemented in two ways
 * Iterative method
 * Recursive method
 *
 * Time complexities
 * 		Best case O(1)
 * 		Average case O(log n)
 * 		Worst case O(log n)
 *
 * Space complexity O(1)
 *
 */

const { range } = require("express/lib/request");
const readline = require("readline-sync");

console.log("enter the size of the list");
let size = Number(readline.question());

console.log("enter the elements into the list");
let nums = [];

for (let i = 0; i < size; i++) {
  nums.push(Number(readline.question()));
}

console.log("enter the target element, which you want to find in the list");
let target = Number(readline.question());

function binarySearch(nums, target) {
  let low = 0,
    high = nums.length - 1;

  while (low <= high) {
    let mid = low + (high - low) / 2;

    if (nums[mid] == target) {
      return mid;
    } else if (target > nums[mid]) {
      low = mid + 1;
    } else high = mid - 1;
  }
  return -1;
}

console.log(binarySearch(nums, target))