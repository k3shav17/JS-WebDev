/*You are typing on a broken keyboard trying to spell your friend’s name. 
 * Since the keyboard is broken, sometimes when you press a key the key is typed one or more times. 
 * Given a string typed and a string name return whether or not you’ve successfully typed your friend’s name even though certain keys might be repeated. 
Note: Both strings will only contain lower case alphabetical characters. 
Ex: Given the following typed and name…
typed = "kkevin", name = "kevin", return true.
Ex: Given the following typed and name…
typed = "timmm", name = "timmy", return false.*/

const readline = require("readline-sync");

console.log("Enter the typed string");

let typed = readline.question();

console.log("enter your friends name");

let name = readline.question();

function isTypedName(typed, name) {
  const typedSet = [...new Set(typed)];
  const nameSet = [...new Set(name)];

  if (typedSet.length === nameSet.length) {
    return true;
  }
  return false;
}

console.log(isTypedName(typed, name));
