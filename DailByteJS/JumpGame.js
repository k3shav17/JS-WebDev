
const req = require('express/lib/request')
const readline = require('readline-sync')
console.log('enter the size of the array')
let size = Number(readline.question())

console.log("enter the elements into the array")
let nums = []
for (let i = 0; i < size; i++) {
    nums.push(Number(readline.question()))

}

function isReachable(nums) {

    let max = 0;

    for (let i = 0; i < nums.length; i++) {
        if (i > max) {
            return 0;
        }
        max = max > (i + nums[i]) ? max : (i + nums[i]);
    }
    return 1;
}

console.log(isReachable(nums))