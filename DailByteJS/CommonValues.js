/*Given two integer arrays, nums1 and nums2, return the intersection of the two arrays (i.e. the elements they have in common).
Ex: Given the following nums1 and nums2...
nums1 = [1, 2, 2, 3], nums2 = [0, 2, 2, 5], return [2, 2].*/

console.log("Intersection of arrays");

const readline = require("readline-sync");

var nums1 = [];
let one = Number(readline.question("enter the size of the first array "));

console.log("enter the elements in to the array");
for (let i = 0; i < one; i++) {
  nums1.push(Number(readline.question()));
}

var nums2 = [];
let two = Number(readline.question("enter the size of the second array "));

console.log("enter the elements in to the array");
for (let i = 0; i < two; i++) {
  nums2.push(Number(readline.question()));
}

function pointOfIntersection(nums1, nums2) {
  var intersection = [];

  for (let i = 0; i < nums1.length; i++) {
    for (let j = i + 1; j < nums2.length; j++) {
      if (nums1[i] == nums2[j]) {
        intersection.push(nums1[i]);
      }
    }
  }
  return intersection;
}
console.log(pointOfIntersection(nums1, nums2));
