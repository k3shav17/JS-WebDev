/*Given an array of integers, nums, and a value k, return the kth largest element.
Ex: Given the following array nums…
[1, 2, 3], k = 1, return 3.
Ex: Given the following array nums…
[9, 2, 1, 7, 3, 2], k = 5, return 2.*/

const readline = require("readline-sync");

console.log("enter the size of the element");
let size = Number(readline.question());

console.log("enter the elements into the array");
let nums = [];

for (let i = 0; i < size; i++) {
  nums.push(Number(readline.question()));
}

console.log("enter the k");
let kth = Number(readline.question());

function kthLargest(nums, kth) {
  let newNums = [];

  for (let i = kth; i < nums.length; i++) {
    newNums.push(nums[i]);
  }
  newNums.sort();
  return newNums[newNums.length - 1];
}
console.log(kthLargest(nums, kth));
