/*Given an array of integers, nums, each element in the array either appears once or twice. Return a list containing all the numbers that appear twice.
Note: Each element in nums is between one and nums.length (inclusive).
Ex: Given the following array nums…
nums = [2, 3, 1, 5, 5], return [5].
Ex: Given the following array nums…
nums = [1, 2, 3], return [].
Ex: Given the following array nums…
nums = [7, 2, 2, 3, 3, 4, 4], return [2,3,4].*/

const readline = require("readline-sync");
console.log("enter the size of the array");
let size = Number(readline.question());

console.log("enter the elements in to the array");
let nums = [];

for (let i = 0; i < size; i++) {
  nums.push(Number(readline.question()));
}

function takingTheDupes(nums) {
  let dupes = [];

  for (let i = 0; i < nums.length; i++) {
    let count = 0;
    for (let j = i + 1; j < nums.length; j++) {
      if (nums[i] == nums[j]) {
        count++;
      }
    }
    if (count >= 1) {
      dupes.push(nums[i]);
    }
  }

  return dupes;
}
console.log(takingTheDupes(nums));
