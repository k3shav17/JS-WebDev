let arr = [];

for (let i = 0; i < 50; i++) {
  arr.push(Math.floor(Math.random() * (50 + 1)));
}
console.log(arr);
let prime = [];
for (let i = 0; i < arr.length; i++) {
  if (isPrime(arr[i])) {
    prime.push(arr[i]);
  }
}

function isPrime(value) {
  if (value == 0 || value == 1) {
    return false;
  }

  for (let i = 2; i < value; i++) {
    if (value % i == 0) {
      return false;
    }
  }
  return true;
}

console.log(prime);
