/*Given an integer, n, return the difference between the product and sum of its digits.
Ex: Given the following n…
n = 321, return 0 ((3 * 2 * 1) - (3 + 2 + 1).
Ex: Given the following n…
n = 56, return 19.*/

console.log(
  "Digit Arithmetic, Difference between the product and sum of individual digits in a number"
);

const readline = require("readline-sync");

let number = Number(readline.question());

let sum = 0,
  product = 1,
  temp = number;

while (temp > 1) {
  temp = temp % 10;
  sum = sum + temp;
  product = product * temp;

  number = number / 10;

  if (number < 1) {
      break;
  }
}
let diff = product - sum;

console.log(diff);
