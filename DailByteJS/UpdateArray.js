/*Given an integer array, nums, replace each element with the largest value that occurs to the right of it and return the array. 
Note: The rightmost element should be replaced with -1. 

Ex: Given the following numsâ€¦

nums = [5, 2, 3], return [3, 3, -1].
Ex: Given the following numsâ€¦

nums = [10, 2, 5, 8, 9], return [9,9,9,9,-1].
*/

const readline = require("readline-sync");
console.log("enter the size of the array");

let size = Number(readline.question());

console.log("enter the elements in to the array");

let nums = [];
for (let i = 0; i < size; i++) {
  nums.push(Number(readline.question()));
}

function update(nums) {
  let updatedNums = [];

  let max = nums[0];
  for (let i = 0; i < nums.length; i++) {
    for (let j = i; j < nums.length; j++) {
      if (nums[j] > max) {
        max = nums[j];
      } else max = -1;
    }
    updatedNums.push(max);
  }
  return updatedNums;
}

console.log(update(nums));
