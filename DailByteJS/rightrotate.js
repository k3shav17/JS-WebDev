/*
 * In this post, we will see how to right-rotate an array by specified positions.
 * For example, right rotating array { 1, 2, 3, 4, 5, 6, 7 } three times will result in array { 5, 6, 7, 1, 2, 3, 4 }.
 */

let nums = [1, 2, 3, 4, 5, 6, 7];
let rot = 3;

function rotation(arr, rot) {
  reverse(arr, nums.length - rot, nums.length - 1);

  reverse(arr, 0, rot);

  reverse(arr, 0, nums.length - 1);
  return arr;
}

function reverse(nums, low, high) {
  for (let i = low, j = high; i < j; i++, j--) {
    swaping(nums, i, j);
  }
}

function swaping(nums, i, j) {
  let temp = nums[i];
  nums[i] = nums[j];
  nums[j] = temp;
}

console.log(rotation(nums, rot));
