/*Given a string s, reverse the words.
Note: You may assume that there are no leading or trailing whitespaces and each word within s is only separated by a single whitespace.
Ex: Given the following string s…
s = "The Daily Byte", return "Byte Daily The".*/

const readline = require("readline-sync");

console.log("enter the sentence which you want to swap or reverse");
let sentence = readline.question();
sentence = sentence.trim();

let onlyWords = sentence.split(/(\s+)/);

let reversedSentence = "";

for (let i = onlyWords.length - 1; i >= 0; i--) {
    reversedSentence += onlyWords[i];
}

console.log(reversedSentence);