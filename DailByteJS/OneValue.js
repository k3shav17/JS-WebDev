/*Given an array of integers, nums, every value appears three times except one value which only appears once. Return the value that only appears once.
Ex: Given the following array nums…
nums = [1, 2, 2, 2, 3, 3, 3], return 1.
Ex: Given the following array nums…
nums = [3, 3, 2, 5, 2, 2, 5, 3, 9, 5], return 9.*/

const readline = require("readline-sync");
console.log("Enter the size of the array");
let size = Number(readline.question());

console.log("enter the elements in to the array");
let nums = [];

for (let i = 0; i < size; i++) {
  nums.push(Number(readline.question()));
}

function oneTime(nums) {
  let value = 0;
  for (let i = 0; i < nums.length; i++) {
    let count = 0;
    for (let j = i + 1; j < nums.length - 1; j++) {
      if (nums[i] === nums[j]) {
        count++;
      }
    }
    if (count <= 1) {
      value = nums[i];
      break;
    }
  }
  return value;
}
console.log(oneTime(nums));
