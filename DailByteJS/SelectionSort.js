const readline = require("readline-sync");

console.log("enter the size of the array");
let size = Number(readline.question());

console.log("enter the elements in to the array");
let nums = [];

for (let i = 0; i < size; i++) {
  nums.push(Number(readline.question()));
}

function selection(nums) {
  for (let i = 0; i < nums.length - 1; i++) {
    let min = i;

    for (let j = i + 1; j < nums.length; j++) {
      if (nums[j] < nums[min]) {
        min = j;
      }
    }

    let temp = nums[i];
    nums[i] = nums[min];
    nums[min] = temp;
  }

  return nums;
}
console.log(selection(nums))