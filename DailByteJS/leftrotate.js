/*
 * In this post, we will see how to left-rotate an array by specified positions.
 * For example, left-rotating array { 1, 2, 3, 4, 5 } twice results in array { 3, 4, 5, 1, 2 }.
 *
 * */

let nums = [1, 2, 3, 4, 5];
let r = 2;

function rotation(nums, rotations) {
  reverse(nums, 0, rotations - 1);

  reverse(nums, rotations, nums.length - 1);

  reverse(nums, 0, nums.length - 1);

  return nums;
}

function reverse(nums, low, high) {
  for (let i = low, j = high; i < j; i++, j--) {
    swaping(nums, i, j);
  }
}

function swaping(nums, i, j) {
  let temp = nums[i];
  nums[i] = nums[j];
  nums[j] = temp;
}

console.log(rotation(nums, r));
