/*Given an integer array that is sorted in ascending order and a value target, 
 * return two unique indices (one based) such that the values at those indices sums to the given target.
Note: If no two such indices exist, return null.
Ex: Given the following nums and target…
nums = [1, 2, 5, 7, 9], target = 10, return [1,5].
Ex: Given the following nums and target…
nums = [1, 3, 8], target = 13, return null.*/

const readline = require("readline-sync");

console.log("enter the size of the array");
let size = Number(readline.question());

console.log("enter the elements into the array");
let nums = [];

for (let i = 0; i < size; i++) {
  nums.push(Number(readline.question()));
}

console.log("enter the target");
let target = Number(readline.question());

function pairIndex(nums, target) {
  var index = [];
  for (let i = 0; i < nums.length - 1; i++) {
    for (let j = i + 1; j < nums.length; j++) {
      if (nums[i] + nums[j] === target) {
        index.push(i + 1);
        index.push(j + 1);
      }
    }
  }
  if (index.length >= 1) return index;

  return null;
}

console.log(pairIndex(nums, target));
