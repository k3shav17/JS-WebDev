/*This question is asked by Amazon. 
 * You are at a birthday party and are asked to distribute cake to your guests. 
 * Each guess is only satisfied if the size of the piece of cake they’re given,
 *  matches their appetite (i.e. is greater than or equal to their appetite). 
 *  Given two arrays, appetite and cake where the ithelement of appetite represents the ith guest’s appetite, 
 *  and the elements of cake represents the sizes of cake you have to distribute, 
 *  return the maximum number of guests that you can satisfy.
Ex: Given the following arrays appetite and cake…
appetite = [1, 2, 3], cake = [1, 2, 3], return 3.
Ex: Given the following arrays appetite and cake…
appetite = [3, 4, 5], cake = [2], return 0.*/

console.log("Welcome to the birthday party.");

const readline = require("readline-sync");

console.log("Enter the no of guests ");
let guests = Number(readline.question());
console.log("Enter their appetites ");
let appetite = [];

for (let i = 0; i < guests; i++) {
  appetite.push(Number(readline.question()));
}

console.log("Enter the no of cake pieces");
let cakePieces = Number(readline.question());
console.log("Enter the respective cake piece");
let cake = [];

for (let i = 0; i < cakePieces; i++) {
  cake.push(Number(readline.question()));
}

// var appetite = [3, 2, 3];
// var cake = [1, 2, 3];

function satisfiedGuests(appetite, cake) {
  let satisfied = 0;

  for (let i = 0; i < appetite.length; i++) {
    for (let j = i; j <= i; j++) {
      if (appetite[i] <= cake[j]) {
        satisfied++;
      }
    }
  }
  return satisfied;
}

console.log(satisfiedGuests(appetite, cake));
