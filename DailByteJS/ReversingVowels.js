//s = "computer", return "cemputor"

console.log("enter the string");
const readline = require("readline-sync");

let string = readline.question();

function reversing(word) {
  let vowels = [];
  let reversedVowels = "";

  for (let i = 0; i < word.length; i++) {
    if (
      word[i] === "a" ||
      word[i] === "e" ||
      word[i] === "i" ||
      word[i] === "o" ||
      word[i] === "u"
    ) {
      vowels.push(word[i]);
    }
  }

  vowels.reverse();

  let index = 0;
  for (let i = 0; i < word.length; i++) {
    if (
      word[i] === "a" ||
      word[i] === "e" ||
      word[i] === "i" ||
      word[i] === "o" ||
      word[i] === "u"
    ) {
      reversedVowels += vowels[index++];
    } else {
      reversedVowels += word[i];
    }
  }

  return reversedVowels;
}

console.log(reversing(string));
