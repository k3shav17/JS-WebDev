/*Given a sorted integer array nums and a target, search for the target with in the array. 
 * If the targets exists within the array, return its index. 
 * If it does not exist within the array, return -1.
Ex: Given the following nums and target...
nums = [-5, -3, 0, 3, 8, 12, 40], target = 8, return 4.
Ex: Given the following nums and target...
nums = [1, 2, 3, 6, 8], target = 10, return -1.*/

const readline = require("readline-sync");

console.log("enter the size of the array");
let size = Number(readline.question());

console.log("enter the elements in to the array");
let nums = [];

for (let i = 0; i < size; i++) {
  nums.push(Number(readline.question()));
}

console.log("enter the target number");
let target = Number(readline.question());

function findingIndex(nums, target) {
  let index = -1;

  for (let i = 0; i < nums.length; i++) {
    if (nums[i] === target) {
      index = i;
      break;
    }
  }
  return index;
}

console.log(findingIndex(nums, target));
