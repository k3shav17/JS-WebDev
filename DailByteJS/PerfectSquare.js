//Given positive an integer num, return whether or not it is a perfect square.

//Ex: Given the following num...

//num = 9, return true.

//Ex: Given the following num...

//num = 18, return false.

const readline = require("readline-sync");
console.log(
  "enter the number that you wants to find whether it is perfect square or not"
);

let number = Number(readline.question());

function isPerfect(number) {
  for (let i = 1; i <= number; i++) {
    if (i * i === number) {
      return true;
    }
  }
  return false;
}

console.log(isPerfect(number));
