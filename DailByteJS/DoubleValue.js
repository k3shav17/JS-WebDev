/*Given an integer array, nums, return true if for any value within nums its double also exists within the array.
Ex: Given the following nums…
nums = [4, 3, 9, 8], return true (4 and 8 both appear in nums).
Ex: Given the following nums…
nums = [9, 2, 3, 5], return false.*/

const readline = require("readline-sync");
console.log("Enter the size of the array");
let size = Number(readline.question());
var nums = [];

console.log("enter the elements in to the array");
for (let i = 0; i < size; i++) {
  nums.push(Number(readline.question()));
}

function doubleOrNot(nums) {
  let flag = false;

  for (let i = 0; i < nums.length; i++) {
    for (let j = i + 1; j < nums.length; j++) {
      if (2 * nums[i] === nums[j]) {
        flag = true;
        break;
      }
    }
  }
  return flag;
}

console.log(doubleOrNot(nums));
