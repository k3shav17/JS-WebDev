//nums = [3, 7, 0, 5, 0, 2], rearrange nums to look like the following [3,7,5,2,0,0]

const readline = require("readline-sync");

console.log("enter the size of the array");

let size = Number(readline.question());

console.log("enter the elements in to the array");
let nums = [];
for (let i = 0; i < size; i++) {
  nums.push(Number(readline.question()));
}

function rearrange(nums) {
  let newList = [];

  for (let i = 0; i < nums.length; i++) {
    if (nums[i] != 0) {
      newList.push(nums[i]);
    }
  }

  for (let i = newList.length; i < nums.length; i++) {
    newList.push(0);
  }
  return newList;
}
console.log(rearrange(nums));
