/*Given a list of nums and a target, return all the unique combinations of nums that sum to target.

Ex: Given the following nums and target...

nums = [8, 2, 2, 4, 5, 6, 3]
target = 9
return [[2,2,5],[2,3,4],[3,6],[4,5]].*/

const readline = require("readline-sync");
console.log("enter the size of the array");

let size = Number(readline.question());

console.log("enter the elements into the array");
let nums = [];
for (let i = 0; i < size; i++) {
  nums.push(Number(readline.question()));
}

console.log("enter the target");
let target = Number(readline.question());

function combinationsToSum(nums, target) {
  let combinations = [];

  for (let i = 0; i < nums.length; i++) {
    let sum = 0;
    let sumArray = [];

    for (let j = 0; j < nums.length; j++) {
      sum = sum + nums[j];
      if (sum === target) {
        break;
      }
      for (let k = 0; k < j; k++) {
        sumArray.push(nums[k]);
      }
    }

    combinations.push(sumArray);
  }

  return combinations;
}

console.log(combinationsToSum(nums, target));
