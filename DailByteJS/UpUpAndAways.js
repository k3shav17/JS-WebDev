// Given an integer array, nums, return whether or not you can make the array strictly non-decreasing by modifying at most one element.
// Note: A non-decreasing array is an array in which nums[i] <= nums[i + 1] for every i.

// Ex: Given the following nums…

// nums = [3, 1, 2], return true (you could modify three to one).

// Ex: Given the following nums…

// nums = [4, 2, 1, 3], return false.

const readline = require('readline-sync')
console.log('enter the size of the array')

let size = Number(readline.question())
console.log('enter the elements into the array')

let nums = [];

for (let i = 0; i < size; i++) {
    nums.push(Number(readline.question()));
}

function isNonDecreasing(nums) {
    let flag = false;
    let count = 0;
    for (let i = 0; i < nums.length - 1; i++) {
        if (nums[i] >= nums[i + 1]) {
           count++; 
        }
    }
    if (count > 1) {
        flag = false;
    } else flag = true;

    return flag;
}

console.log(isNonDecreasing(nums))