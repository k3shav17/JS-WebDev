/*
 * Given a string s and an integer array, indices, 
 * you must rearrange s according to the given indices. 
 * Once you have rearranged s successfully, the ith character in s should now be located at the indices[i] index.
 * Note: s and indices will always be the same length.
Ex: Given the following s and indices…
s = "abc", indices = [2, 1, 0], return "cba" ('a' moves to the 2nd index, b stays at the first index, and c moves to the zeroth index).
*/

const readline = require("readline-sync");
console.log("enter the string");
let word = readline.question();

console.log("enter the size of the array");
let size = Number(readline.question());

console.log("enter the indices you want to change in the given word");
let indices = [];

for (let i = 0; i < size; i++) {
  indices.push(Number(readline.question()));
}

function rearrange(word, indices) {
  let rearrangeStr = "";

  for (let i = 0; i < indices.length; i++) {
    rearrangeStr += word[indices[i]];
  }

  return rearrangeStr;
}

console.log(rearrange(word, indices));
