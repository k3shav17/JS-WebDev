/*Given an integer array, A, return a new array representing the same number with K added to it.

Ex: Given the following A and K...

A = [1, 2, 3], K = 10, return [1, 3, 3].
Ex: Given the following A and K...

A = [9], K = 3, return [1, 2].*/

const readline = require("readline-sync");

console.log("enter the size of the array");

let size = Number(readline.question());
var array = [];
console.log("enter the elements in to the array");

for (let i = 0; i < size; i++) {
  array.push(Number(readline.question()));
}

console.log("enter the target value");
let target = Number(readline.question());

function sumElements(array, target) {
  let str = "",
    number = 0;
  var newArr = [];

  for (let i = 0; i < array.length; i++) {
    str += array[i];
  }

  number = Number(str);
  number = number + target;

  str = number + "";
  for (let i = 0; i < str.length; i++) {
    newArr.push(Number(str.charAt(i)));
  }

  return newArr;
}

console.log(sumElements(array, target));
