/*Given an integer array, nums, return true if there are three consecutive odd values within nums, otherwise, return false.
Ex: Given the following nums…
nums = [1, 2, 3, 4, 5], return false.
Ex: Given the following nums…
nums = [1, 3, 4, 2, 3, 9, 15], return true.*/

const readline = require("readline-sync");
console.log("enter the size of the array");

let size = Number(readline.question());
console.log("enter the values in to the array");

let nums = [];

for (let i = 0; i < size; i++) {
  nums.push(Number(readline.question()));
}

function checkingTheOdds(nums) {
  let flag = false;

  for (let i = 0; i < nums.length - 2; i++) {
    if (nums[i] % 2 != 0 && nums[i + 1] % 2 != 0) {
      if (nums[i + 2] % 2 != 0) {
        flag = true;
      }
    }
  }

  return flag;
}
console.log(checkingTheOdds(nums));
