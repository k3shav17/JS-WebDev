const readline = require("readline-sync");
console.log('enter the word that you want to remove vowels in it')
let word = readline.question();

function onlyConsonants(word) {
  let withoutVowels = "";

  for (let i = 0; i < word.length; i++) {
    if (
      !(
        word[i] === "a" ||
        word[i] === "e" ||
        word[i] === "i" ||
        word[i] === "o" ||
        word[i] === "u"
      )
    ) {
      withoutVowels += word[i];
    }
  }
  return withoutVowels;
}
console.log(onlyConsonants(word.toLocaleLowerCase()));
