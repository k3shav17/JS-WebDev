/*Given a number n, find the highest power of 2 that is smaller than or equal to n.

Examples : 

Input : n = 10
Output : 8

Input : n = 19
Output : 16

Input : n = 32
Output : 32*/

const readline = require("readline-sync");
console.log(
  "enter the number for which you wants to know the power of two less than the given number"
);

let number = Number(readline.question());

function powerOfTwo(number) {
  let product = 1;

  for (let i = 1; i < number; i++) {
    if (product * 2 <= number) {
      product = product * 2;
    }
  }

  return product;
}

console.log(powerOfTwo(number));
