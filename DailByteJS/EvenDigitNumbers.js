/*Given an integer array, nums, return the total number of integers within nums that have an even number of digits.
Ex: Given the following nums…
nums = [1, 12, 123], return 1 (12 is the only integer with an even number of digits).
Ex: Given the following nums…
nums = [1, 32, 3492, 23], return 3*/

const readline = require("readline-sync");

console.log("enter the size of the array");

let size = Number(readline.question());

var nums = [];

for (let i = 0; i < size; i++) {
  nums.push(readline.question());
}

function evenLength(nums) {
  let count = 0;

  for (let i = 0; i < nums.length; i++) {
    if (nums[i].length % 2 == 0) {
      count++;
    }
  }
  return count;
}

console.log(evenLength(nums));
