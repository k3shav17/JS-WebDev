const size = 5;

function pascalsTriangle(size) {
  let pascals = [];
  let firstRow = [];

  firstRow.push(1);
  pascals.push(firstRow);

  for (let i = 1; i < size; i++) {
    let previousRow = pascals[i - 1];
    let nextRow = [];

    nextRow.push(1);

    for (let j = 1; j < i; j++) {
      nextRow.push(previousRow[j - 1] + previousRow[j]);
    }
    nextRow.push(1);
    pascals.push(nextRow);
  }

  return pascals;
}

console.log(pascalsTriangle(5));
