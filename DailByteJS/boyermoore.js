/*
 * Given an integer array containing duplicates, return the majority element if present.
 * A majority element appears more than n/2 times, where n is the array size.
 * For example, the majority element is 2 in array {2, 8, 7, 2, 2, 5, 2, 3, 1, 2, 2}.
 * */

let array = [2, 8, 7, 2, 2, 5, 2, 3, 1, 2, 2];

function findingMajority(array) {
  var map = new Map();

  for (let i = 0; i < array.length; i++) {
    map.set(array[i], map.has(array[i]) + 1);
  }

  console.log(map.has(array[0]) + 1);
  for (const [k, v] of map.entries()) {
    if (v > array.length / 2);
    return `Majority element in the array is ${v}`;
  }
}

console.log(findingMajority(array));
