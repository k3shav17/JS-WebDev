/*Given an array, nums, every value appears twice except for one which only appears once. 
 * The value that only appears once is the lucky number. Return the lucky number.
Ex: Given the following nums…
nums = [1, 2, 1], return 2.
Ex: Given the following nums…
nums = [1, 3, 1, 2, 2], return 3.*/

const readline = require("readline-sync");

console.log("enter the size of the array");
let size = Number(readline.question());

console.log("Enter the elements in to the array");
let nums = [];
for (let i = 0; i < size; i++) {
  nums.push(Number(readline.question()));
}

let luckyNum = (nums) => {
  let num = 0;
  for (let i = 0; i < nums.length; i++) {
    let count = 0;
    for (let j = 0; j < nums.length; j++) {
      if (nums[i] === nums[j]) {
        count++;
      }
    }
    if (count == 1) {
      num = nums[i];
    }
  }
  return num;
};
console.log(luckyNum(nums));
